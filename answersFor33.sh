#!/usr/bin/env bash
#############################
#created by: MaorN
#purpose: Advanced file permissions
############################

#Set up a directory, owned by the group sports.
groupadd sports
mkdir /home/sport
chown root:sports /root/home

# Members of the sports group should be able to create files in this directory
chmod 770 /home/sports

# All files created in this directory should be group-owned by the sports group.
chmod 2770 /home/sports

# 1d. Users should be able to delete only their own user-owned files
chmod +t /home/sports

#

