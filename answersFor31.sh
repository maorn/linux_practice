#!/usr/bin/env bash
#############################
#created by: MaorN
#purpose: practice- groups
############################

#Create the groups tennis, football and sports.
groupadd tennis
groupadd football
groupadd sports

#In one command, make venus a member of tennis and sports
usermod -a -G tennis,sports venus
#venus appended sports and tennis as secondary groups

#Rename the football group to foot.
groupmod -n foot football

#
#vigr -> add serena to tennis group

tail -5 /etc/group
ball_club:x:1009:betty
betty:x:1011:
tennis:x:1012:venus, serena #serena has been added
sports:x:1014:venus
foot:x:1013:

#Use the id command to verify that serena is a member of tennis.
id serena | grep tennis
#gid=1005(serena) groups=1005(serena),1012(tennis)

#Make someone responsible for managing group membership of foot and sports. Test that it works.
gpasswd -A serena sports
gpasswd -A serena tennis
#now serena is the admin of sports and tennis group

#test:
su - serena
id venus
#gid=1006(venus) groups=1006(venus),1012(tennis),1014(sports)

gpasswd -d venus tennis
#Removing user venus from group tennis

id venus
#gid=1006(venus) groups=1006(venus),1014(sports)