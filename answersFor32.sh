#!/usr/bin/env bash
#############################
#created by: MaorN
#purpose:practice- standard file permissions
############################

#As normal user, create a directory ~/permissions. Create a file owned by yourself in there.
mkdir ~/permissions
touch ~/permissions/someFile

#Copy a file owned by root from /etc/ to your permissions dir, who owns this file now?
cp /etc/passwd ~/permissions

#As root, create a file in the users ~/permissions directory.
sudo su - root
touch f1 /home/student/permissions/f1

#As normal user, look at who owns this file created by root.
su - student
ls -l permissions/
#root is the owner

#Change the ownership of all files in ~/permissions to yourself
chown student permissions/*
#cant change roots file and still read only

# Make sure you have all rights to these files, and others can only read.
chmod 744 permissions/*

# With chmod, is 770 the same as rwxrwx--- ?
#YES.

# With chmod, is 664 the same as r-xr-xr-- ?
#NO, but 551.

#With chmod, is 400 the same as r-------- ?
#YES.

#With chmod, is 734 the same as rwxr-xr-- ?
#NO, but 754.

#
umask
#0022
umask -S