#!/usr/bin/env bash
#############################
#created by: MaorN
#purpose: Practice- User passwords
############################

Set the password for serena to hunter2.
passwd serena
#set a new password:
hunter2
#new password again
hunter2

#Also set a password for venus and then lock the venus user account with usermod. Verify the locking in /etc/shadow before and after you lock it.
passwd venus
#set a new password:
hunter2
#new password again
hunter2
#->
su - venus
#(password is needed)
exit 
#->
grep venus /etc/shadow | cut -c1-70
usermod -L venus
grep venus /etc/shadow | cut -c1-70
su - venus

#Use passwd -d to disable the serena password. Verify the serena line in /etc/shadow before and after disabling.
grep serena /etc/shadow | cut -c1-70
passwd -d serena
grep serena /etc/shadow | cut -c1-70

#What is the difference between locking a user account and disabling a user account's password like we just did with usermod -L and passwd -d?

#Try changing the password of serena to serena as serena.
##cant change the password to 'serena' - the password is shoter than 8 characters.

#Make sure serena has to change her password in 10 days.
chage -M 10 serena 

#Make sure every new user needs to change their password every 10 days.
vi /etc/login.defs #->
PASS_MAX_DAYS 10

#Take a backup as root of /etc/shadow. Use vi to copy an encrypted hunter2 hash from
venus to serena. Can serena now log on with hunter2 as a password ?


#Why use vipw instead of vi ? What could be the problem when using vi or vim ?

#Use chsh to list all shells (only works on RHEL/CentOS/Fedora), and compare to cat /etc/shells.
chsh -l
#/bin/sh
#/bin/bash
#/usr/bin/sh
#/usr/bin/bash
#/bin/tcsh
#/bin/csh


cat /etc/shells
#/bin/sh
#/bin/bash
#/usr/bin/sh
#/usr/bin/bash
#/bin/tcsh
#/bin/csh

#Which useradd option allows you to name a home directory ?
man useradd
 
 -d, --home-dir
 the new user will be created using HOME DIR as the value for the user's login directory. ..

#How can you see whether the password of user serena is locked or unlocked ? Give a solution with grep and a solution with passwd.

grep serena /etc/shadow

passwd -S serena
