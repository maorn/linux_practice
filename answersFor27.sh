#!/usr/bin/env bash
#############################
#created by: MaorN
#purpose: practice- introduction to users
############################

#Run a command that displays only your currently logged on user name
whoami

#Display a list of all logged on users.
who

#Display a list of all logged on users including the command they are running at this very moment.
w

#Display your user name and your unique user identification (userid).
id

#Use su to switch to another user account (unless you are root, you will need the password of the other account). And get back to the previous account.
su --> exit

#Now use su - to switch to another user and notice the difference. Note that su - gets you into the home directory of Tania.
su - --> pwd -> /home/student
#the user has been change but the dir didnt

#Try to create a new user account (when using your normal user account). this should fail. (Details on adding user accounts are explained in the next # chapter.)
cant create a new user
/usr/sbin/useradd picklerick

#Now try the same, but with sudo before your command.
sudo /usr/sbin/useradd picklerick
sudo su - picklerick
pwd -> /home/picklerick
