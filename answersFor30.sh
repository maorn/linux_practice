#!/usr/bin/env bash
#############################
#created by: MaorN
#purpose: practice- user profiles
############################

#Make a list of all the profile files on your system.
ls -la /etc/profile* /etc/bash*

#Read the contents of each of these, often they source extra scripts.

#Put a unique variable, alias and function in each of those files.
#alias turtle= 'echo Mutants'

#Try several different ways to obtain a shell (su, su -, ssh, tmux, gnome-terminal, Ctrlalt-F1, ...) and verify which of your custom variables,
# aliases and function are present in your environment.

#Do you also know the order in which they are executed?

#When an application depends on a setting in $HOME/.profile, does it matter whether $HOME/.bash_profile exists or not ?