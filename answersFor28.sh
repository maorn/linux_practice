#!/usr/bin/env bash
#############################
#created by: MaorN
#purpose: practice- user management
############################

#Create a user account named serena, including a home directory and a description (or 
# comment) that reads Serena Williams. Do all this in one single command.
useradd -m -d /home serena -c "Serena Williamd" serena

#Create a user named venus, including home directory, bash shell, a description that reads
# Venus Williams all in one single command.
useradd -m -d /home venus -c "venus Williams" venus

#Verify that both users have correct entries in /etc/passwd, /etc/shadow and /etc/group.
tail -2 /etc/passwd
tail -2 /etc/shadow
tail -2 /etc/group

#Verify that their home directory was created.
ls -lrt /home

#Create a user named einstime with /bin/date as his default logon shell.
useradd -s $(which date) einstime

#What happens when you log on with the einstime user ? Can you think of a useful real
# world example for changing a user's login shell to an application ?
su - einstime

#Create a file named welcome.txt and make sure every new user will see this file in their home directory.
echo Welcome > /etc/skel/welcome.txt

#Verify this setup by creating (and deleting) a test user account.
useradd -m newUser
su - newUser
ls

#Change the default login shell for the serena user to /bin/sh. Verify before and after you make this change.
usermod -s /bin/bash serena